#[macro_use] extern crate log;
use std::error::Error;
use gumdrop::Options;

use rogl::dbus::InterfaceProxy;


#[derive(Options, Debug)]
struct ARGSParse {
    #[options(help = "print help message")]
    help: bool,
    #[options(help = "show program version number")]
    version: bool,
    #[options(help = "get current profile name")]
    get_profile: bool,
    #[options(help = "switch to next profile")]
    next_profile: bool,
    #[options(help = "init current profile profile")]
    init_profile: bool,
}


pub fn main() -> Result<(), Box<dyn Error>> {
    std::env::set_var("RUST_LOG", "debug");
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();

    
    let opts = ARGSParse::parse_args_default_or_exit();
    let connection = zbus::Connection::new_system()?;
    let proxy = InterfaceProxy::new(&connection)?;

    if opts.get_profile {
        let (_, profile) = proxy.get_profile().unwrap();
        info!("profile: {:?}", profile);
        println!("{}", profile);
    }

    if opts.next_profile {
        let profile = proxy.next_profile();
        info!("profile: {:?}", profile);
    }

    if opts.init_profile {
        let profile = proxy.init_profile();
        info!("profile: {:?}", profile);
    }
    
    Ok(())
}
