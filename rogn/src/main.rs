#[macro_use] extern crate log;

use std::error::Error;

use gio::prelude::*;

mod lib;


pub fn main() -> Result<(), Box<dyn Error>> {
    

    

    std::env::set_var("RUST_LOG", "debug");
    std::env::set_var("RUST_BACKTRACE", "1");
    
    env_logger::init();

    info!("starting up");

    //let connection = zbus::Connection::new_system().expect("failed creating dbus connection");
    //let dbus_proxy = InterfaceProxy::new(&connection).expect("failed creating dbus interface");
    //let dbus_proxy = Rc::new(RefCell::new(dbus_proxy));
    let application = gtk::Application::new(Some("org.rogwatcher.Daemonv2"), Default::default()).expect("Initialization failed...");

    
    {
        //let app_dbus_proxy = dbus_proxy.clone();
        application.connect_activate(move |app| {
            let notification = lib::notification::Notification::new(app);
            notification.main_loop();
        });

        application.run(&vec![]);
    }
    Ok(())
}
