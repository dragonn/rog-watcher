use std::thread;

use bindkey::*;
use device_query::keymap::Keycode;

use crate::lib;

//#[derive(Debug)]
pub struct Events(Option<CallbackStorage>);

impl Events {
    pub fn new() -> Self {
        let new = Self(Some(CallbackStorage::new()));
        new
    }

    pub fn add(&mut self, keycode: Keycode, callback: Box<dyn Fn() -> ()>) {
        if let Some(storage) = &mut self.0 {
            let key = match keycode {
                Keycode::Meta => keysym::XK_Meta_L,
                Keycode::Prog1 => keysym::XF86XK_Launch1,
                Keycode::Prog4 => keysym::XF86XK_Launch4,
                _ => {
                    error!("unsupported key code: {:?}", keycode);
                    return;
                }
            };

            storage.add(&HotKey::new(key, vec![], TriggerOn::Press), callback);
        } else {
            error!("thread already started!!");
        }
    }

    pub fn start(&mut self) {
        Self::thread(self.0.take().unwrap());
    }

    fn thread(storage: CallbackStorage) {
        let handler = thread::spawn(move || {
            info!("event thread started!!");
            start(storage);
        });
    }
}
