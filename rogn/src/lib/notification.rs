use gtk::prelude::*;

use device_query::{DeviceQuery, DeviceState, Keycode};

use shlex;

use std::error::Error;
use std::rc::Rc;
use std::sync::Arc;
use std::{cell::RefCell, sync::Mutex};

use std::process::Command;

use std::fs;
use std::io::Read;

use crate::lib;
use rogl::dbus::{client::Client, InterfaceProxy};

#[derive(Clone)]
pub struct Notification {
    window: gtk::ApplicationWindow,
    vbox: gtk::Box,

    svg_image: gtk::Image,
    label: gtk::Label,

    timeout_source: Rc<RefCell<Option<glib::source::SourceId>>>,
}

impl Notification {
    pub fn new(application: &gtk::Application) -> Self {
        let window = gtk::ApplicationWindow::new(application);
        let screen = window.get_screen().ok_or("failed get screen handle").unwrap();

        window.set_visual(screen.get_rgba_visual().as_ref());

        window.set_position(gtk::WindowPosition::CenterAlways);
        window.set_decorated(false);
        window.set_modal(true);
        window.set_keep_above(true);
        window.set_accept_focus(false);
        window.set_app_paintable(true);
        window.set_skip_taskbar_hint(true);
        window.set_skip_pager_hint(true);
        window.stick();

        let svg_image = gtk::Image::new();
        svg_image.set_opacity(1.0);

        window.set_title("Rogoverlay2");
        window.set_border_width(10);

        window.set_position(gtk::WindowPosition::Center);
        window.set_default_size(200, 200);

        let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);

        window.add(&vbox);

        window.connect_draw(move |_, cr| {
            cr.set_source_rgba(0.0, 0.0, 0.0, 0.6);
            cr.paint();
            gtk::Inhibit(false)
        });

        let svg_image = gtk::Image::new();
        svg_image.set_opacity(1.0);

        //let pixbuf = Self::get_image_data(&image);
        //svg_image.set_from_pixbuf(Some(&pixbuf));

        vbox.pack_start(&svg_image, true, false, 0);

        let label = gtk::Label::new(Some(""));
        label.set_justify(gtk::Justification::Center);
        vbox.pack_start(&label, true, false, 0);

        Notification {
            window,
            vbox,

            svg_image,
            label,

            timeout_source: Rc::new(RefCell::new(None)),
        }
    }

    pub fn update_notafiction(&mut self, dbus_proxy: &Client) {
        if let Ok(notification) = dbus_proxy.get_notification() {
            match Self::get_image_data(&notification.icon) {
                Ok(pixbuf) => self.svg_image.set_from_pixbuf(Some(&pixbuf)),
                Err(err) => {
                    error!("problem when loading icon: {} {:?}", notification.icon, err);
                    self.svg_image.clear();
                }
            };

            self.label.set_text(&notification.title);
            debug!("notification: {:?}", notification);
            self.timeout(5);
            self.show();
        }
    }

    pub fn get_user_command(&mut self, dbus_proxy: &Client) {
        //TODO: find a better solution for that....
        while let Ok(user_command) = dbus_proxy.get_user_command() {
            info!("got user command: {:?}", user_command);
            let mut user_command_split = shlex::split(&user_command).unwrap();
            let user_spawn = user_command_split.remove(0);

            match Command::new(&user_spawn).args(&user_command_split).spawn() {
                Ok(mut child) => {
                    if let Err(err) = child.wait() {
                        error!("command: {} wait error: {:?}", user_command, err)
                    }
                }
                Err(err) => error!("command: {} spawn error: {:?}", user_command, err),
            };
            std::thread::sleep(std::time::Duration::from_millis(5));
        }
    }

    pub fn main_loop(&self) {
        let mut config = lib::config::Config::new();
        config.interfaces.retain(|_, v| v.is_key());
        info!("config: {:?}", config);

        let dbus_proxy = Client::new();
        let dbus_proxy_clone = dbus_proxy.clone();
        let g_self = Rc::new(RefCell::new(self.clone()));
        glib::source::timeout_add_local(300, move || {
            let mut g_self = g_self.borrow_mut();
            g_self.update_notafiction(&dbus_proxy_clone);
            glib::Continue(true)
        });

        let g_self = Rc::new(RefCell::new(self.clone()));
        let dbus_proxy_clone = dbus_proxy.clone();
        glib::source::timeout_add_local(150, move || {
            let mut g_self = g_self.borrow_mut();
            g_self.get_user_command(&dbus_proxy_clone);
            glib::Continue(true)
        });

        let dbus_proxy_clone = dbus_proxy.clone();
        let g_self = Rc::new(RefCell::new(self.clone()));

        let profile_key = config.profile_key;
        let interfaces = config.interfaces;
        let device_state = DeviceState::new();
        let mut old_keys: Vec<Keycode> = device_state.get_keys();
        let mut events = lib::events::Events::new();
        let got_keyevent = Arc::new(Mutex::new(false));

        let got_keyevent_clone = got_keyevent.clone();
        events.add(
            profile_key,
            Box::new(move || {
                dbus_proxy_clone.next_profile();
                *got_keyevent_clone.lock().unwrap() = true;
            }),
        );

        for (name, interface) in &interfaces {
            if let Some(watch) = &interface.watch {
                if let Some(key) = &watch.key {
                    info!("adding interface: {} key press: {}", name, key);
                    let name = name.clone();
                    let dbus_proxy_clone = dbus_proxy.clone();
                    let got_keyevent_clone = got_keyevent.clone();
                    events.add(
                        key.clone(),
                        Box::new(move || {
                            dbus_proxy_clone.next_interface(&name);
                            *got_keyevent_clone.lock().unwrap() = true;
                        }),
                    );
                }
            }
        }
        events.start();

        let dbus_proxy_clone = dbus_proxy.clone();
        glib::source::timeout_add_local(5, move || {
            let mut got_keyevent_lock = got_keyevent.lock().unwrap();
            if *got_keyevent_lock {
                let mut g_self = g_self.borrow_mut();
                g_self.update_notafiction(&dbus_proxy_clone);
                *got_keyevent_lock = false;
            }
            glib::Continue(true)
        });
    }

    pub fn show(&self) {
        self.window.show_all();
    }

    fn get_image_data(path: &str) -> Result<gdk_pixbuf::Pixbuf, Box<dyn Error>> {
        let path = format!("/usr/share/icons/rog-ctrl/{}", path);

        let mut f = fs::File::open(&path)?;
        let metadata = fs::metadata(&path)?;
        let mut buffer = vec![0; metadata.len() as usize];
        f.read(&mut buffer)?;

        let input_stream = gio::MemoryInputStream::from_bytes(&glib::Bytes::from(&buffer));
        let cancable: Option<gio::Cancellable> = None;
        Ok(gdk_pixbuf::Pixbuf::from_stream_at_scale(&input_stream, 200 - 50, 200 - 50, true, cancable.as_ref())?)
    }

    pub fn timeout(&mut self, timeout: u32) {
        let mut source = self.timeout_source.borrow_mut();
        let window = self.window.clone();

        if let Some(source) = source.take() {
            glib::source::source_remove(source);
        }

        let clear_source = self.timeout_source.clone();
        let timeout_source = glib::source::timeout_add_local(timeout * 1000, move || {
            window.hide();
            let mut clear_source = clear_source.borrow_mut();
            *clear_source = None;
            glib::Continue(false)
        });

        *source = Some(timeout_source);
    }
}
