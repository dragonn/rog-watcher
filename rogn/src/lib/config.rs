use std::str::FromStr;
use std::collections::HashMap;

use device_query::keymap::Keycode;
use serde::{de, Deserialize, Deserializer};
use serde_json;

use rogl::config::read_config;



#[derive(Deserialize, Debug)]
pub struct InterfaceWatch {
    #[serde(deserialize_with = "option_keycode_from_str")]
    pub key: Option<Keycode>
}

#[derive(Deserialize, Debug)]
pub struct Interface {
    pub watch: Option<InterfaceWatch>,
}

impl Interface {
    pub fn is_key(&self) -> bool {
        if let Some(watch) = &self.watch {
            if let Some(_) = &watch.key {
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    pub fn pressed(&self, keys: &Vec<Keycode>) -> bool {
        let mut pressed = false;
        if let Some(watch) = &self.watch {
            if let Some(key) = &watch.key {
               pressed = keys.contains(key); 
            }
        }
        pressed
    }
}

#[derive(Deserialize, Debug)]
pub struct Config {
    pub interfaces: HashMap<String, Interface>,
    #[serde(deserialize_with = "keycode_from_str")]
    pub profile_key: Keycode,
}

fn keycode_from_str<'de, D>(deserializer: D) -> Result<Keycode, D::Error>
    where D: Deserializer<'de> 
{
    let s = String::deserialize(deserializer)?;
    Keycode::from_str(&s).map_err(de::Error::custom)
}

fn option_keycode_from_str<'de, D>(deserializer: D) -> Result<Option<Keycode>, D::Error>
    where D: Deserializer<'de> 
{
    let s = String::deserialize(deserializer)?;
    Ok(Some(Keycode::from_str(&s).map_err(de::Error::custom)?))
}

impl Config {
    pub fn new() -> Self {
        serde_json::from_str(&read_config()).expect("error while reading json")
    }
}

