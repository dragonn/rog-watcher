use std::str::FromStr;

use serde::{de, Deserialize, Deserializer};

use device_query::keymap::Keycode;

#[derive(Deserialize)]
#[serde(untagged)]
pub enum KeycodeOptions {
    #[serde(deserialize_with = "keycode_from_str")]
    One(Keycode),
    #[serde(deserialize_with = "vec_keycode_from_str")]
    Combination(Vec<Keycode>),
    #[serde(deserialize_with = "vec_vec_keycode_from_str")]
    MutipleCombination(Vec<Vec<Keycode>>),
}

fn keycode_from_str<'de, D>(deserializer: D) -> Result<Keycode, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    //Ok(KeycodeOptions::One(Keycode::from_str(&s).map_err(de::Error::custom)?))
    Keycode::from_str(&s).map_err(de::Error::custom)
}

fn vec_keycode_from_str<'de, D>(deserializer: D) -> Result<Vec<Keycode>, D::Error>
where
    D: Deserializer<'de>,
{
    let mut out_v = vec![];
    let de_v: Vec<String> = Vec::deserialize(deserializer)?;
    for e in de_v {
        out_v.push(Keycode::from_str(&e).map_err(de::Error::custom)?);
    }

    Ok(out_v)
}

fn vec_vec_keycode_from_str<'de, D>(deserializer: D) -> Result<Vec<Vec<Keycode>>, D::Error>
where
    D: Deserializer<'de>,
{
    let mut out_v: Vec<Vec<Keycode>> = vec![];
    let de_v: Vec<Vec<String>> = Vec::deserialize(deserializer)?;
    for v in de_v {
        let mut part_v = vec![];
        for e in v {
            part_v.push(Keycode::from_str(&e).map_err(de::Error::custom)?);
        }
        out_v.push(part_v)
    }

    Ok(out_v)
}

impl KeycodeOptions {
    pub fn pressed(&self, keys: &Vec<Keycode>) -> bool {
        match self {
            Self::One(check_key) => keys.contains(check_key),
            Self::Combination(check_keys) => {
                for check_key in check_keys {
                    if !keys.contains(check_key) {
                        return false;
                    }
                }
                true
            }
            Self::MutipleCombination(check_vec_keycodes) => {
                for check_keys in check_vec_keycodes {
                    let mut contains = true;
                    for check_key in check_keys {
                        if !keys.contains(check_key) {
                            contains = false;
                        }
                    }
                    if contains {
                        return true;
                    }
                }
                false
            }
        }
    }
}
