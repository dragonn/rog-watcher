VERSION := $(shell grep -Pm1 'version = "(\d.\d.\d)"' rogd/Cargo.toml | cut -d'"' -f2)

INSTALL = install
INSTALL_PROGRAM = ${INSTALL} -D -m 0755
INSTALL_DATA = ${INSTALL} -D -m 0644

prefix = /usr
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
datarootdir = $(prefix)/share
libdir = $(exec_prefix)/lib
zshcpl = $(datarootdir)/zsh/site-functions

BIN_C := rogc
BIN_D := rogd
BIN_N := rogn
BIN_G := rogg

DBUS_RULES := rog-ctrl.conf
CONFIG := config.json
GPU := gpu.json

SYSTEMD_ROGD := rogd.service
SYSTEMD_ROGG := rogg.service

XORG_INTEGRATED := integrated.conf
XORG_HYBRID := hybrid.conf
XORG_NVIDIA := nvidia.conf

ICON_FAN_FULL = full.png
ICON_FAN_SILENT = silent.png
ICON_FAN_NORMAL = normal.png
ICON_FAN_BOOST = boost.png


ICON_PROFILES_AC = ac.png
ICON_PROFILES_BATTERY = battery.png
ICON_PROFILES_ROG = rog.png

SRC := Cargo.toml Cargo.lock Makefile $(shell find -type f -wholename '**/src/*.rs')

DEBUG ?= 0
ifeq ($(DEBUG),0)
	ARGS += --release
	TARGET = release
endif

VENDORED ?= 0
ifeq ($(VENDORED),1)
	ARGS += --frozen
endif

all: build

clean:
	cargo clean

distclean:
	rm -rf .cargo vendor vendor.tar.xz

install:
	$(INSTALL_PROGRAM) "./target/release/$(BIN_C)" "$(DESTDIR)$(bindir)/$(BIN_C)"
	$(INSTALL_PROGRAM) "./target/release/$(BIN_D)" "$(DESTDIR)$(bindir)/$(BIN_D)"
	$(INSTALL_PROGRAM) "./target/release/$(BIN_N)" "$(DESTDIR)$(bindir)/$(BIN_N)"
	$(INSTALL_PROGRAM) "./target/release/$(BIN_G)" "$(DESTDIR)$(bindir)/$(BIN_G)"
	
	$(INSTALL_DATA) "./data/dbus/$(DBUS_RULES)" "$(DESTDIR)$(datarootdir)/dbus-1/system.d/$(DBUS_RULES)"
	$(INSTALL_DATA) "./data/$(CONFIG)" "$(DESTDIR)etc/rog-ctrl/$(CONFIG)"
	$(INSTALL_DATA) "./data/$(GPU)" "$(DESTDIR)etc/rog-ctrl/$(GPU)"
	
	$(INSTALL_DATA) "./data/xorg/$(XORG_INTEGRATED)" "$(DESTDIR)etc/rog-ctrl/xorg/$(XORG_INTEGRATED)"
	$(INSTALL_DATA) "./data/xorg/$(XORG_HYBRID)" "$(DESTDIR)etc/rog-ctrl/xorg/$(XORG_HYBRID)"
	$(INSTALL_DATA) "./data/xorg/$(XORG_NVIDIA)" "$(DESTDIR)etc/rog-ctrl/xorg/$(XORG_NVIDIA)"
	
	$(INSTALL_DATA) "./data/systemd/$(SYSTEMD_ROGD)" "$(DESTDIR)usr/lib/systemd/system/$(SYSTEMD_ROGD)"
	$(INSTALL_DATA) "./data/systemd/$(SYSTEMD_ROGG)" "$(DESTDIR)usr/lib/systemd/system/$(SYSTEMD_ROGG)"

	$(INSTALL_DATA) "./data/icons/fan/$(ICON_FAN_FULL)" "$(DESTDIR)usr/share/icons/rog-ctrl/fan/$(ICON_FAN_FULL)"
	$(INSTALL_DATA) "./data/icons/fan/$(ICON_FAN_SILENT)" "$(DESTDIR)usr/share/icons/rog-ctrl/fan/$(ICON_FAN_SILENT)"
	$(INSTALL_DATA) "./data/icons/fan/$(ICON_FAN_NORMAL)" "$(DESTDIR)usr/share/icons/rog-ctrl/fan/$(ICON_FAN_NORMAL)"
	$(INSTALL_DATA) "./data/icons/fan/$(ICON_FAN_BOOST)" "$(DESTDIR)usr/share/icons/rog-ctrl/fan/$(ICON_FAN_BOOST)"

	$(INSTALL_DATA) "./data/icons/profiles/$(ICON_PROFILES_AC)" "$(DESTDIR)usr/share/icons/rog-ctrl/profiles/$(ICON_PROFILES_AC)"
	$(INSTALL_DATA) "./data/icons/profiles/$(ICON_PROFILES_BATTERY)" "$(DESTDIR)usr/share/icons/rog-ctrl/profiles/$(ICON_PROFILES_BATTERY)"
	$(INSTALL_DATA) "./data/icons/profiles/$(ICON_PROFILES_ROG)" "$(DESTDIR)usr/share/icons/rog-ctrl/profiles/$(ICON_PROFILES_ROG)"
	
	
uninstall:
	rm -f "$(DESTDIR)$(bindir)/$(BIN_C)"
	rm -f "$(DESTDIR)$(bindir)/$(BIN_D)"
	rm -f "$(DESTDIR)$(bindir)/$(BIN_N)"
	
	rm -f "$(DESTDIR)$(datarootdir)/dbus/system.d/$(DBUS_RULES)"
	
	rm -rf "$(DESTDIR)etc/rog-ctrl"

	rm -f "$(DESTDIR)usr/lib/systemd/system/$(SYSTEMD_ROGD)"
	rm -f "$(DESTDIR)usr/lib/systemd/system/$(SYSTEMD_ROGG)"

	rm -rf "$(DESTDIR)usr/share/icons/rog-ctrl"

update:
	cargo update

vendor:
	mkdir -p .cargo
	cargo vendor | head -n -1 > .cargo/config
	echo 'directory = "vendor"' >> .cargo/config
	mv .cargo/config ./cargo-config
	rm -rf .cargo
	tar pcfJ vendor_rog-framework_$(VERSION).tar.xz vendor
	rm -rf vendor

build:
ifeq ($(VENDORED),1)
	@echo "version = $(VERSION)"
	tar pxf vendor_rog-framework_$(VERSION).tar.xz
endif
	cargo build $(ARGS)

.PHONY: all clean distclean install uninstall update build
