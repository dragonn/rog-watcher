use std::fs;
use std::str::FromStr;
use serde_json::json;

#[derive(Deserialize, Debug)]
pub struct Profile {
    pub name: String,
    logic: serde_json::Value,
}

#[derive(Deserialize, Debug)]
pub struct Config {
    pub search_usb: bool,
    pub auto: Vec<Profile>,
}

impl Config {
    pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
        #[cfg(debug_assertions)]
        let config_str = fs::read_to_string("gpu.json")?;
        #[cfg(not(debug_assertions))]
        let config_str = fs::read_to_string("/etc/rog-ctrl/gpu.json")?;

        Ok(serde_json::from_str(&config_str)?)
    }
}

impl Profile {
    pub fn test(&self, data: &serde_json::Value) -> bool {
        match jsonlogic::apply(&self.logic, data) {
            Ok(test) => {
                if let serde_json::Value::Bool(test) = test {
                    test
                } else {
                    warn!("rule for profile: {:?} didn't produce bool but: {:?}", self, test);
                    false
                }
            },
            Err(err) => {
                warn!("rule for profile: {:?} failed: {:?}", self, err);
                false
            }
        }
    }
}