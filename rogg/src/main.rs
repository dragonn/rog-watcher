#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;

use std::fs;
use std::os::unix::fs::symlink;
use std::process::Command;
use std::str::FromStr;
use std::thread::{sleep, spawn};

use std::time::Duration;

use strum::{AsRefStr, EnumString};

use serde_json::json;

use chrono::prelude::*;

use gumdrop::Options;

mod config;

#[derive(Debug, PartialEq, EnumString, AsRefStr, Clone)]
enum Mode {
    #[strum(serialize = "integrated")]
    Integrated,
    #[strum(serialize = "hybrid")]
    Hybrid,
    #[allow(non_camel_case_types)]
    #[strum(serialize = "nvidia")]
    nVidia,
    #[strum(serialize = "auto")]
    Auto,
}

impl Mode {
    fn apply(&self) {
        info!("setting mode: {}", self.as_ref());

        if *self != Mode::Auto {
            let mode = self.clone();
            let handle = spawn(move || {
                if let Err(err) = fs::remove_file("/etc/X11/xorg.conf.d/01-gpu.conf") {
                    warn!("failed removing linked xorg conf: {:?}", err);
                }

                if let Err(err) = symlink(format!("/etc/rog-ctrl/xorg/{}.conf", mode.as_ref()), "/etc/X11/xorg.conf.d/01-gpu.conf") {
                    error!("failed linking new config: {:?}, exiting", err);
                    #[cfg(not(debug_assertions))]
                    return;
                }

                if let Err(err) = fs::write("/sys/bus/pci/devices/0000:01:00.0/power/control", "auto\n") {
                    warn!("failed setting dGPU power control: {}", err);
                }
            });

            match self {
                Mode::Hybrid | Mode::nVidia => Mode::load_nvidia(),
                Mode::Integrated => Mode::remove_nvidia(),
                _ => error!("something is wrong, mode auto schould not fall into this"),
            };

            handle.join();
        } else {
            Self::auto_mode();
        }
    }

    fn auto_ac() -> bool {
        if let Ok(online) = fs::read_to_string("/sys/class/power_supply/AC0/online") {
            online == "1\n"
        } else {
            false
        }
    }

    fn auto_mode() {
        info!("running auto mode!!");
        let mode = match config::Config::new() {
            Ok(config) => {
                let usb_devices = if config.search_usb {
                    let mut usb_devices: Vec<String> = usb_enumeration::enumerate(None, None).into_iter().map(|u| format!("{:04x}:{:04x}", u.vendor_id, u.product_id)).collect();
                    loop {
                        sleep(Duration::from_millis(100));
                        let mut new_usb_devices: Vec<String> = usb_enumeration::enumerate(None, None).into_iter().map(|u| format!("{:04x}:{:04x}", u.vendor_id, u.product_id)).collect();
                        if new_usb_devices.len() == usb_devices.len() {
                            break;
                        } else {
                            usb_devices = new_usb_devices;
                        }
                    }
                    usb_devices
                } else {
                    vec![]
                };

                let now: DateTime<Local> = Local::now();

                let data = json!({
                    "usb_devices": usb_devices,
                    "ac": Self::auto_ac(),
                    "hour": now.hour(),
                    "weekday": now.weekday().number_from_monday(),
                });

                info!("data: {}", data);
                let mut mode = Mode::Integrated;
                for profile in config.auto {
                    if profile.test(&data) {
                        if let Ok(parsed_mode) = Mode::from_str(&profile.name) {
                            if parsed_mode != Mode::Auto {
                                mode = parsed_mode;
                                break;
                            } else {
                                warn!("auto mode is not allowed in profiles, this will lead to a recursion")
                            }
                        }
                    }
                }
                mode
            }
            Err(err) => {
                error!("error loading gpu config: {:?}, falling back to integrated mode", err);
                Mode::Integrated
            }
        };

        mode.apply();
    }

    fn load_nvidia() {
        info!("loading dGPU modules");
        if let Err(err) = fs::write("/sys/bus/pci/rescan", "1\n") {
            error!("failed rescaning the dGPU: {:?}", err);
        }
        std::thread::sleep(std::time::Duration::from_millis(100));
        Command::new("modprobe").arg("nvidia_drm").arg("modeset=0").arg("nvidia_modeset").arg("nvidia_uvm").output().expect("modprobe call failed");
        Command::new("modprobe").arg("nvidia").output().expect("modprobe call failed");
    }

    fn remove_nvidia() {
        info!("removing dGPU");
        if let Err(err) = fs::write("/sys/bus/pci/devices/0000:01:00.0/remove", "1\n") {
            error!("failed removing dGPU: {:?}", err);
        }
    }
}

#[derive(Debug, Options)]
struct CmdOptions {
    #[options(help = "print help message")]
    help: bool,

    #[options(help = "select a gpu mode")]
    mode: Option<Mode>,
}
fn main() {
    let opts = CmdOptions::parse_args_default_or_exit();

    std::env::set_var("RUST_LOG", "debug");
    std::env::set_var("RUST_BACKTRACE", "1");

    env_logger::init();

    let mode = if let Some(mode) = opts.mode {
        mode
    } else {
        let mut mode = Mode::Auto;

        for f in fs::read_to_string("/proc/cmdline").unwrap().split_whitespace() {
            //println!("f: {}", f);
            if f.starts_with("rogg.gpu=") {
                let modestr = f.split("=").skip(1).next();
                //println!("modestr: {:?}", modestr);
                if let Some(modestr) = modestr {
                    if let Ok(modeparsed) = Mode::from_str(modestr) {
                        mode = modeparsed;
                    }
                }
            }
        }
        mode
    };

    mode.apply();
}
