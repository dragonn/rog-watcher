use std::collections::HashMap;

use serde_json;

use rogl::config::read_config;

use crate::lib::{interface::Interface, profiles::Profiles};

#[derive(Deserialize, Debug)]
pub struct Auto {
    pub interface: String,
    pub profiles: HashMap<String, Vec<String>>,
}

#[derive(Deserialize, Debug)]
pub struct Config {
    pub interfaces: HashMap<String, Interface>,
    pub auto: Auto,
    pub profiles: HashMap<String, Profiles>,
    #[serde(default)]
    pub hyperx_cloud: Option<HyperXConfig>,
}

#[derive(Deserialize, Debug)]
pub struct HyperXConfig {
    pub mute_action: String,
    pub unmute_action: String,
    pub poweron_action: String,
    pub poweroff_action: String,
    pub volumeup_action: String,
    pub volumedown_action: String,
}

impl Config {
    pub fn new() -> Self {
        serde_json::from_str(&read_config()).expect("error while reading json")
    }
}
