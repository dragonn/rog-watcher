use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;

use gumdrop::Opt;
use hyperx_cloud::{Event, HyperXCloud as HyperXCloudInterface, Mute, Power, Volume};

use crate::config::HyperXConfig;

#[derive(Debug)]
pub struct HyperXCloud {
    receiver: Receiver<Event>,
    config: HyperXConfig,
}

pub struct HyperXEvent {
    pub user_command: Option<String>,
}

impl HyperXEvent {
    pub fn new() -> Self {
        Self { user_command: None }
    }
}

impl HyperXCloud {
    pub fn new(config: HyperXConfig) -> Self {
        info!("got hyperx config: {:?}", config);
        let (sender, receiver) = channel();
        let new = Self { receiver, config };
        Self::spawn(sender);
        new
    }

    pub fn event(&self) -> HyperXEvent {
        let mut procesed_event = HyperXEvent::new();

        if let Some(event) = self.receiver.try_recv().ok() {
            match event {
                Event::Mute(mute) => match mute {
                    Mute::Muted => procesed_event.user_command = Some(self.config.mute_action.clone()),
                    Mute::Unmuted => procesed_event.user_command = Some(self.config.unmute_action.clone()),
                },
                Event::Power(power) => match power {
                    Power::On => procesed_event.user_command = Some(self.config.poweron_action.clone()),
                    Power::Off => procesed_event.user_command = Some(self.config.poweroff_action.clone()),
                },
                Event::Volume(volume) => match volume {
                    Volume::Up => procesed_event.user_command = Some(self.config.volumeup_action.clone()),
                    Volume::Down => procesed_event.user_command = Some(self.config.volumedown_action.clone()),
                },
                Event::Battery(_) => {}
                _ => {
                    warn!("ignored event! {:?}", event);
                }
            }
        }

        procesed_event
    }

    fn spawn(sender: Sender<Event>) {
        thread::spawn(move || {
            //info!("hyper x thread spawned");
            let mut interface = HyperXCloudInterface::new(30);
            loop {
                let event = interface.listen();
                //info!("got event: {:?}", event);
                sender.send(event).expect("sending hyperx even failed");
            }
        });
    }
}
