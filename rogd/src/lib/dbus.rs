use std::convert::TryInto;
use std::sync::mpsc;
use std::thread;

use serde::Serialize;
use zbus::{dbus_interface, fdo};

use rogl::dbus::{Notification, Result as DbusResult};

#[derive(Debug)]
pub enum DbusComamnd {
    InitProfile,
    NextProfile,
    GetProfile,
    NextInterface(String),
}

#[derive(Debug, Serialize)]
pub enum DbusData {
    Profile(String),
    //Notafiction(String),
}

pub struct DbusIntreface {
    txcommand: mpsc::Sender<DbusComamnd>,
    rxdata: mpsc::Receiver<DbusData>,
    rxnotification: mpsc::Receiver<Notification>,
    rxusercommand: mpsc::Receiver<String>,
}

#[dbus_interface(name = "org.rog_ctrl.Daemon")]
impl DbusIntreface {
    fn next_profile(&mut self) {
        self.txcommand.send(DbusComamnd::NextProfile).unwrap();
    }

    fn init_profile(&mut self) {
        self.txcommand.send(DbusComamnd::InitProfile).unwrap();
    }

    #[allow(irrefutable_let_patterns)]
    fn get_profile(&mut self) -> DbusResult<String> {
        self.txcommand.send(DbusComamnd::GetProfile).unwrap();
        if let DbusData::Profile(profile) = self.rxdata.recv().expect("main thread died") {
            (true, profile)
        } else {
            (false, String::new())
        }
    }

    fn get_notification(&mut self) -> DbusResult<Notification> {
        match self.rxnotification.try_recv() {
            Ok(notafiction) => (true, notafiction),
            Err(err) => match err {
                mpsc::TryRecvError::Empty => (false, Notification::default()),
                _ => {
                    panic!("Dbus server died");
                }
            },
        }
    }

    fn next_interface(&mut self, interface: String) {
        self.txcommand.send(DbusComamnd::NextInterface(interface)).unwrap();
    }

    #[allow(irrefutable_let_patterns)]
    fn get_user_command(&mut self) -> DbusResult<String> {
        match self.rxusercommand.try_recv() {
            Ok(usercommand) => (true, usercommand),
            Err(err) => match err {
                mpsc::TryRecvError::Empty => (false, String::new()),
                _ => {
                    panic!("Dbus server died");
                }
            },
        }
    }
}

#[derive(Debug)]
pub struct DbusChannel {
    pub rxcommand: mpsc::Receiver<DbusComamnd>,
    pub txdata: mpsc::Sender<DbusData>,
    pub txnotification: mpsc::Sender<Notification>,
    pub txusercommand: mpsc::Sender<String>,
}

#[derive(Debug)]
pub struct DbusServer {}

impl DbusServer {
    pub fn new() -> Self {
        Self {}
    }

    pub fn start(&self) -> DbusChannel {
        let (txnotification, rxnotification) = mpsc::channel();
        let (txcommand, rxcommand) = mpsc::channel();
        let (txdata, rxdata) = mpsc::channel();
        let (txusercommand, rxusercommand) = mpsc::channel();
        thread::spawn(move || {
            let connection = zbus::Connection::new_system().unwrap();
            fdo::DBusProxy::new(&connection).unwrap().request_name("org.rog_ctrl.Daemon", fdo::RequestNameFlags::ReplaceExisting.into()).unwrap();

            let mut object_server = zbus::ObjectServer::new(&connection);
            let dbus = DbusIntreface {
                txcommand,
                rxdata,
                rxnotification,
                rxusercommand,
            };
            object_server.at(&"/org/rog_ctrl/Daemon".try_into().unwrap(), dbus).unwrap();

            loop {
                if let Err(err) = object_server.try_handle_next() {
                    error!("{}", err);
                }
            }
        });
        DbusChannel {
            rxcommand,
            txdata,
            txnotification,
            txusercommand,
        }
    }
}
