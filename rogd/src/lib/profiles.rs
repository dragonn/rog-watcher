use std::collections::HashMap;
use std::process::Command;
use std::thread;

use shlex;

use super::interface::{Interface, InterfacePath};

#[derive(Deserialize, Debug)]
pub struct Profiles {
    pub icon: String,
    pub desc: String,
    interfaces: HashMap<String, String>,
    commands: Vec<String>,
    user_commands: Vec<String>,
}

impl Profiles {
    pub fn apply_interfaces(&self, intefaces: &HashMap<String, Interface>) {
        for (inteface, value) in &self.interfaces {
            //info!("{:?}, {:?}", inteface, value);
            let tempinterface = Interface::new(InterfacePath::Str(inteface.to_owned()));
            let interface = match intefaces.get(inteface) {
                Some(inteface) => inteface,
                None => &tempinterface,
            };

            interface.write(value);
        }
    }

    pub fn apply_commands(&self) {
        for command in &self.commands {
            let command = command.clone();
            thread::spawn(move || {
                let mut command_split = shlex::split(&command).unwrap();
                let spawn = command_split.remove(0);
                match Command::new(&spawn).args(&command_split).spawn() {
                    Ok(mut child) => {
                        if let Err(err) = child.wait() {
                            error!("command: {} wait error: {:?}", command, err)
                        }
                    }
                    Err(err) => error!("command: {} spawn error: {:?}", command, err),
                };
            });
        }
    }

    pub fn user_commands(&self) -> Vec<String> {
        self.user_commands.clone()
    }
}
