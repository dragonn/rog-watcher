use std::fs;
use std::error::Error;
use std::collections::HashMap;

use bimap::BiBTreeMap;
use glob::glob;

use rogl::dbus::Notification;

#[derive(Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum InterfacePath {
    Str(String),
    StrArray(Vec<String>),
}

#[derive(Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum IconType {
    Str(String),
    Map(HashMap<String, String>),
}

#[derive(Deserialize, Debug, Clone)]
pub struct InterfaceWatch {
    icon: IconType,
    desc: String,

    #[serde(skip)]
    old_value: Option<String>,
}

impl InterfaceWatch {
    pub fn check(&mut self, new_value: Result<String, Box<dyn Error>>) -> Option<Notification> {
        match new_value {
            Ok(new_value) => {
                if let Some(old_value) = &self.old_value {
                    if old_value != &new_value {
                        let icon = match &self.icon {
                            IconType::Str(icon) => icon,
                            IconType::Map(map) => {
                                match map.get(&new_value) {
                                    Some(icon) => icon,
                                    None => {
                                        error!("icon for: {} not found in map: {:?}", new_value, map);
                                        return None
                                    },
                                }
                            }
                        };
                        info!("new value: {}", new_value);
                        let notification = Some(Notification::new(&format!("{} {}", &self.desc, &new_value), &icon));
                        self.old_value = Some(new_value);
                        notification
                    } else {
                        None
                    }
                } else {
                    self.old_value = Some(new_value);
                    None
                }
            },
            Err(_) => None
        }
    }

    pub fn clear(&mut self) {
        self.old_value = None;
    }
}


#[derive(Deserialize, Debug, Clone)]
pub struct Interface {
    path: InterfacePath,
    states: Option<BiBTreeMap<String, String>>,
    pub watch: Option<InterfaceWatch>,
}

impl Interface {
    pub fn new(path: InterfacePath) -> Self {
        Self {
            path,
            states: None,
            watch: None,
        }
    }

    pub fn is_watch(&self) -> bool {
        if let Some(_) = self.watch {
            true
        } else {
            false
        }
    }

    pub fn next(&self) {
        if let Ok(current_value) = self.read_value() {
            match &self.states {
                Some(states) => { 
                    let state_values: Vec<(&String, &String)> = states.iter().collect();
                    let found_index = state_values.iter().position(|&r| r.1 == &current_value).expect("current value not in state");
                    let next_value = state_values.get((found_index + 1) % state_values.len()).expect("next value not in state");
                    self.write(next_value.1);
                },
                None => error!("swithing to next value is only supported by interface with states!")
            };
        } else {
            error!("error reading current value when swithing, ingnoring");
        }
    }


    fn get_patches(&self) -> Vec<String> {
        let mut defpatches = vec![];
        let defpatches = match &self.path {
            InterfacePath::Str(path) => {defpatches.push(path.to_owned()); &defpatches},
            InterfacePath::StrArray(patches) => patches
        };

        let mut foundpatches = vec![];
        for path in defpatches {
            for path in glob(path).expect("Failed to read glob pattern") {
                let path = path.unwrap();
                let path = path.to_str().unwrap().to_owned();
                foundpatches.push(path);
            }
        }

        foundpatches
    }

    pub fn read_value(&self) -> Result<String, Box<dyn Error>> {
        let pathes = self.get_patches();
        let path = pathes.get(0).ok_or("no patch was found")?;

        let value = fs::read_to_string(&path)?.trim_end().to_owned();
        match &self.states {
            Some(states) => {
                match states.get_by_left(&value.to_owned()) {
                    Some(value) => Ok(value.to_owned()),
                    None => { 
                        error!("value: {:?} not found in states: {:?}", value, states);
                        Err(format!("value: {:?} not found in states: {:?}", value, states).to_owned().into())
                    }
                }
            }
            None => Ok(value)
        }
    }

    fn write_value(&self, value: &str) {
        for path in self.get_patches() {
            match fs::write(&path, format!("{}\n", value)) {
                Ok(_) => (),
                Err(error) => error!("error writing value msg: {:?} to: {}", error, path)
            };
        }
    }

    pub fn write(&self, value: &str) {
        match &self.states {
            Some(states) => {
                match states.get_by_right(&value.to_owned()) {
                    Some(value) => self.write_value(value),
                    None => error!("value: {:?} not found in states: {:?}", value, states)
                };
            }
            None => self.write_value(value)
        };
    }
}