use std::collections::HashMap;
use std::error::Error;

use serde_json;

use crate::config::Config;

use rogl::config::{read_save, write_save};

#[derive(Serialize, Deserialize, Debug)]
pub struct Save(HashMap<String, String>);

impl Save {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        Ok(serde_json::from_str(&read_save()?)?)
    }

    pub fn fallback(config: &Config) -> Self {
        let mut map = HashMap::new();
        for (value, profiles) in &config.auto.profiles {
            if let Some(profile) = profiles.get(0) {
                map.insert(value.to_string(), profile.to_string());
            }
        }
        Self(map)
    }

    pub fn get(&self, state: &str) -> Option<&String> {
        self.0.get(state)
    }

    pub fn set(&mut self, state: &str, profile: &str) {
        self.0.insert(state.to_string(), profile.to_string());
        write_save(&serde_json::to_string(&self).unwrap()).expect("error writing save to file");
    }
}