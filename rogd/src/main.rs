#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate log;

use std::collections::HashMap;
use std::error::Error;
use std::sync::mpsc;
use std::thread;
use std::time::{Duration, Instant};

use rogl::dbus::Notification;

mod config;
mod lib;
mod save;

use crate::config::Config;
use crate::lib::{
    dbus::{DbusChannel, DbusComamnd, DbusData, DbusServer},
    interface::Interface,
};
use crate::save::Save;

use hyperx_cloud::Event as HyperXEvent;

pub fn main() -> Result<(), Box<dyn Error>> {
    #[cfg(debug_assertions)]
    std::env::set_var("RUST_LOG", "debug");
    #[cfg(debug_assertions)]
    std::env::set_var("RUST_BACKTRACE", "1");

    env_logger::init();

    println!("Starting daemon");
    Daemon::new().start();

    Ok(())
}

#[derive(Debug)]
pub struct Daemon {
    config: Config,
    save: Save,

    profiles: Vec<String>,
    current_profile: String,

    auto_interface: Interface,
    auto_value: String,

    dbusserver: DbusServer,
    dbuschannel: DbusChannel,

    watch_interfaces: HashMap<String, Interface>,

    hyperxcloud: Option<lib::hyperxcloud::HyperXCloud>,
}

impl Daemon {
    pub fn new() -> Self {
        let mut config = Config::new();

        let save = if let Ok(save) = Save::new() { save } else { Save::fallback(&config) };

        let auto_interface = config.interfaces.get(&config.auto.interface).unwrap().clone();

        let dbusserver = DbusServer::new();
        let dbuschannel = dbusserver.start();

        let mut watch_interfaces = HashMap::new();

        for (name, inteface) in &config.interfaces {
            if inteface.is_watch() {
                watch_interfaces.insert(name.to_owned(), inteface.clone());
            }
        }

        let mut hyperxcloud = None;
        if let Some(hyperx_cloud) = config.hyperx_cloud.take() {
            hyperxcloud = Some(lib::hyperxcloud::HyperXCloud::new(hyperx_cloud));
        } else {
            warn!("hyperx interface disabled");
        }

        Self {
            config,
            save,

            profiles: vec![],
            current_profile: String::new(),

            auto_interface,
            auto_value: String::new(),

            dbusserver,
            dbuschannel,

            watch_interfaces,

            hyperxcloud,
        }
    }

    fn apply_profile(&mut self) {
        let profile = self.config.profiles.get(&self.current_profile).expect(&format!("profile: {} not found in profiles", self.current_profile));
        profile.apply_interfaces(&self.config.interfaces);
        profile.apply_commands();
        self.dbuschannel.txnotification.send(Notification::new(&profile.desc, &profile.icon)).unwrap();

        for interface in self.watch_interfaces.values_mut() {
            interface.watch.as_mut().unwrap().clear();
        }

        for user_command in profile.user_commands() {
            self.dbuschannel.txusercommand.send(user_command).expect("dbus server died");
        }
    }

    pub fn start(&mut self) {
        let mut last_activity = Instant::now();
        loop {
            if let Some(hyperxcloud) = &self.hyperxcloud {
                let proccesed_event = hyperxcloud.event();
                if let Some(user_command) = proccesed_event.user_command {
                    self.dbuschannel.txusercommand.send(user_command).expect("dbus server died");
                    last_activity = Instant::now();
                }
            }

            let command = match self.dbuschannel.rxcommand.try_recv() {
                Ok(command) => Some(command),
                Err(err) => match err {
                    mpsc::TryRecvError::Empty => None,
                    _ => {
                        panic!("Dbus server died");
                    }
                },
            };

            if let Some(command) = &command {
                //info!("got command: {:?}", command);
                match command {
                    DbusComamnd::NextProfile => {
                        let current = self.profiles.iter().position(|x| x == &self.current_profile).ok_or("not found current profile").unwrap();
                        self.current_profile = self.profiles.iter().cycle().skip(current + 1).next().unwrap().to_string();
                        info!("next profile: {}", self.current_profile);
                        self.apply_profile();
                        self.save.set(&self.auto_value, &self.current_profile);

                        last_activity = Instant::now();
                    }
                    DbusComamnd::InitProfile => {
                        self.apply_profile();
                        last_activity = Instant::now();
                    }
                    DbusComamnd::GetProfile => {
                        self.dbuschannel.txdata.send(DbusData::Profile(self.current_profile.clone())).expect("dbus server died");
                    }
                    DbusComamnd::NextInterface(interface) => {
                        if let Some(interface) = self.watch_interfaces.get_mut(interface) {
                            info!("switching interface: {:?}", interface);
                            interface.next();
                        } else {
                            error!("interface: {} not found!", interface);
                        }
                        last_activity = Instant::now();
                    }
                };
            }

            let auto_value = self.auto_interface.read_value().unwrap();
            if auto_value != self.auto_value {
                self.auto_value = auto_value;
                self.profiles = self.config.auto.profiles.get(&self.auto_value).unwrap().clone();
                self.current_profile = match self.save.get(&self.auto_value) {
                    Some(saved) => {
                        info!("saved: {:?}", saved);
                        match self.profiles.iter().find(|&x| x == saved) {
                            Some(saved) => saved.to_string(),
                            None => self.profiles.get(0).unwrap().to_string(),
                        }
                    }
                    None => self.profiles.get(0).unwrap().to_string(),
                };

                self.apply_profile();
                last_activity = Instant::now();
            }

            for interface in self.watch_interfaces.values_mut() {
                //debug!("interface: {:?}", interface);
                let new_value = interface.read_value();
                let watch = &mut interface.watch.as_mut().unwrap();
                if let Some(notification) = watch.check(new_value) {
                    debug!("notification: {:?}", notification);
                    self.dbuschannel.txnotification.send(notification).unwrap();
                    last_activity = Instant::now();
                }
            }

            if last_activity.elapsed().as_millis() > 2500 {
                thread::sleep(Duration::from_millis(150));
            } else {
                //info!("running on high power");
                thread::sleep(Duration::from_millis(5));
            }
        }
    }
}
