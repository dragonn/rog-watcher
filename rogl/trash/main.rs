use gio::prelude::*;
use gtk::prelude::*;

use std::env::args;
use std::fs;
use std::io::Read;
use std::thread::spawn;

mod notification;

pub struct Notification {
    window: gtk::ApplicationWindow,
    vbox: gtk::Box, 

    svg_image: gtk::Image,
    label: gtk::Label,
    
    visiable: bool,
}

impl Notification {
    pub fn new(application: &gtk::Application, image: &str, label: &str) -> Self {
        let window = gtk::ApplicationWindow::new(application);
        let screen = window.get_screen().ok_or("failed get screen handle").unwrap();

        window.set_visual(screen.get_rgba_visual().as_ref());

        window.set_position(gtk::WindowPosition::CenterAlways);
        window.set_decorated(false);
        window.set_modal(true);
        window.set_keep_above(true);
        window.set_accept_focus(false);
        window.set_app_paintable(true);
        window.set_skip_taskbar_hint(true);
        window.set_skip_pager_hint(true);
        window.stick();

        let svg_image = gtk::Image::new();
        svg_image.set_opacity(1.0);

        window.set_title("Rogoverlay2");
        window.set_border_width(10);

        window.set_position(gtk::WindowPosition::Center);
        window.set_default_size(200, 200);

        let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);

        window.add(&vbox);

        window.connect_draw(move|_, cr|{
            cr.set_source_rgba(0.0, 0.0, 0.0, 0.6);
		    cr.paint();
            gtk::Inhibit(false)
        });



        
        let svg_image = gtk::Image::new();
        svg_image.set_opacity(1.0);

        let pixbuf = Self::get_image_data(&image);
        svg_image.set_from_pixbuf(Some(&pixbuf));

        vbox.pack_start(&svg_image, true, false, 0);

        let label = gtk::Label::new(Some(label));
		label.set_justify(gtk::Justification::Center);
		vbox.pack_start(&label, true, false, 0);

        Notification {
            window,
            vbox,

            svg_image,
            label,

            visiable: false,
        }
    }

    pub fn show(&mut self) {
        self.visiable = true;
        self.window.show_all();
        let label = self.label.clone();
        let svg_image = self.svg_image.clone();
        glib::source::timeout_add_local(2 * 1000, move || {
            label.set_text("wtf");
            let pixbuf = Self::get_image_data("battery.png");
            svg_image.set_from_pixbuf(Some(&pixbuf));
            glib::Continue(false)
        });
    }

    fn get_image_data(path: &str) -> gdk_pixbuf::Pixbuf {
        let path = format!("/etc/rog-watcher/{}", path);

        let mut f = fs::File::open(&path).expect("no file found");
        let metadata = fs::metadata(&path).expect("unable to read metadata");
        let mut buffer = vec![0; metadata.len() as usize];
        f.read(&mut buffer).expect("buffer overflow");

        let input_stream = gio::MemoryInputStream::from_bytes(&glib::Bytes::from(&buffer));
        let cancable: Option<gio::Cancellable> = None;
        gdk_pixbuf::Pixbuf::from_stream_at_scale(&input_stream, 200 - 50, 200 - 50, true, cancable.as_ref()).expect("failed creating pixbuf")
    }

    pub fn timeout(&self, timeout: u32) {
        let window = self.window.clone();
        glib::source::timeout_add_local(timeout * 1000, move || {
            window.close();
            glib::Continue(false)
        });
    }
}

fn main() {
    spawn(|| {
        let application =
        gtk::Application::new(Some("com.github.gtk-rs.examples.basic"), Default::default())
            .expect("Initialization failed...");
        
        application.connect_activate(|app| {
            let mut notification = Notification::new(app, "rog.png", "rog test");
            //build_ui(app, "rog.png");
            
            //notification.load_image("rog.png");
            //notification.set_label("rog test");
            notification.timeout(5);
            notification.show();
        });

        application.run(&args().collect::<Vec<_>>());
    }).join().unwrap(); 
}