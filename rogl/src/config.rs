use std::fs;
use std::error::Error;

pub fn read_config() -> String {
    #[cfg(debug_assertions)]
    let result = fs::read_to_string("config.json");
    #[cfg(not(debug_assertions))]
    let result = fs::read_to_string("/etc/rog-ctrl/config.json");

    result.expect("config file not found!").parse().expect("error parsing config file")
}

pub fn read_save() -> Result<String, Box<dyn Error>> {
    #[cfg(debug_assertions)]
    let result = fs::read_to_string("save.json");
    #[cfg(not(debug_assertions))]
    let result = fs::read_to_string("/etc/rog-ctrl/save.json");

    Ok(result?.parse()?)
}

pub fn write_save(data: &str) -> Result<(), Box<dyn Error>>  {
    #[cfg(debug_assertions)]
    fs::write("save.json", data)?;
    #[cfg(not(debug_assertions))]
    fs::write("/etc/rog-ctrl/save.json", data)?;

    Ok(())
}