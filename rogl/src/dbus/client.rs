use std::rc::Rc;

use super::{InterfaceProxy, Notification};

#[derive(Clone)]
pub struct Client<'a> {
    proxy: Rc<InterfaceProxy<'a>>,
}

impl Client<'_> {
    pub fn new() -> Self {
        let connection = zbus::Connection::new_system().expect("failed creating dbus connection");
        let proxy = InterfaceProxy::new(&connection).expect("failed creating dbus interface");

        Self { proxy: Rc::new(proxy) }
    }

    pub fn next_profile(&self) {
        self.proxy.next_profile().expect("failed sending next profile");
    }

    pub fn init_profile(&self) {
        self.proxy.init_profile().expect("failed sending init profile");
    }

    pub fn get_profile(&self) -> Result<String, ()> {
        if let Ok((status, profile)) = self.proxy.get_profile() {
            if status {
                Ok(profile)
            } else {
                Err(())
            }
        } else {
            Err(())
        }
    }

    pub fn get_notification(&self) -> Result<Notification, ()> {
        //info!("got notafiction");
        if let Ok((status, notifiction)) = self.proxy.get_notification() {
            if status {
                Ok(notifiction)
            } else {
                Err(())
            }
        } else {
            Err(())
        }
    }

    pub fn next_interface(&self, inteface: &str) {
        self.proxy.next_interface(inteface).expect("failed sending next interface");
    }

    pub fn get_user_command(&self) -> Result<String, ()> {
        if let Ok((status, profile)) = self.proxy.get_user_command() {
            if status {
                Ok(profile)
            } else {
                Err(())
            }
        } else {
            Err(())
        }
    }
}
