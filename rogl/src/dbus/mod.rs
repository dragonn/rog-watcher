use serde::{Deserialize, Serialize};

use zbus::dbus_proxy;
use zvariant_derive::Type;

pub mod client;

pub type Result<T> = (bool, T);

#[derive(Debug, Deserialize, Serialize, Type, Default)]
pub struct Notification {
    pub title: String,
    pub icon: String,
}

impl Notification {
    pub fn new(title: &str, icon: &str) -> Self {
        Self {
            title: title.to_string(),
            icon: icon.to_string(),
        }
    }
}

#[dbus_proxy(interface = "org.rog_ctrl.Daemon", default_service = "org.rog_ctrl.Daemon", default_path = "/org/rog_ctrl/Daemon")]
pub trait Interface {
    /// Switch to next profile in current list
    fn next_profile(&self) -> zbus::Result<()>;

    /// Init current profile
    fn init_profile(&self) -> zbus::Result<()>;

    /// Get current profile name
    fn get_profile(&self) -> zbus::Result<Result<String>>;

    /// Get pending notafictions
    fn get_notification(&self) -> zbus::Result<Result<Notification>>;

    /// Switch to next value in a interface
    fn next_interface(&self, interface: &str) -> zbus::Result<()>;

    /// Get some user command to execute
    fn get_user_command(&self) -> zbus::Result<Result<String>>;
}
